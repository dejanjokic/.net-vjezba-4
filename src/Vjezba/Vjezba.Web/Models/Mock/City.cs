﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Ninject;

namespace Vjezba.Web.Models.Mock
{
    public class City
    {
        public int ID { get; set; }
        public string PostalCode { get; set; }
        public string Name { get; set; }

        [Inject]
        private MockCompanyRepository _companyRepo { get; set; }

        private List<Company> _companies;
        [ScriptIgnore]
        public ICollection<Company> Companies
        {
            get
            {
                return _companies ?? (_companies = _companyRepo.All().Where(p => p.CityID == ID).ToList());
            }
        }
    }

}