﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Ninject;
using Ninject.Web.Common;

namespace Vjezba.Web.Models.Mock
{
    public class MockCityRepository
    {
        private List<City> _cache;

        public static MockCityRepository GetInstance()
        {
            IKernel kernel = new Bootstrapper().Kernel;
            return kernel.Get<MockCityRepository>();
        }

        public IQueryable<City> All()
        {
            if (_cache != null)
                return _cache.AsQueryable();

            var xmlPath = HttpContext.Current.Server.MapPath("~/App_Data/MockData.xml");
            var xDoc = XDocument.Load(xmlPath);

            var allNodes = xDoc.Root.Descendants("Company")
                .Select(p => new City()
                {
                    ID = Math.Abs(p.Descendants("City").First().Value.GetHashCode()),
                    Name = p.Descendants("City").First().Value,
                    PostalCode = p.Descendants("PostalCode").First().Value
                })
                .GroupBy(p => p.ID)
                .Select(p => p.First())
                .AsQueryable();

            _cache = allNodes.ToList();

            return allNodes;
        }

        public City FindByID(int cityId)
        {
            return All().Where(p => p.ID == cityId)
                .FirstOrDefault();
        }

        public bool InsertOrUpdate(City entity)
        {
            //This is just mock repository
            return true;
        }

        public bool Delete(int cityId)
        {
            //This is just mock repository
            return true;
        }
    }

}