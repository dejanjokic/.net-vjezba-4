﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vjezba.Web.Models.Mock
{
    public class Company
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime DateFrom { get; set; }

        public int CityID { get; set; }
        public City City { get; set; }

    }

}