﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using Ninject;
using Ninject.Web.Common;

namespace Vjezba.Web.Models.Mock
{
    public class MockCompanyRepository
    {
        private List<Company> _cache;

        [Inject]
        private MockCityRepository _cityRepository { get; set; }

        public static MockCompanyRepository GetInstance()
        {
            IKernel kernel = new Bootstrapper().Kernel;
            return kernel.Get<MockCompanyRepository>();
        }

        public IQueryable<Company> All()
        {
            if (_cache != null)
                return _cache.AsQueryable();

            var xmlPath = HttpContext.Current.Server.MapPath("~/App_Data/MockData.xml");
            var xDoc = XDocument.Load(xmlPath);

            var allNodes = xDoc.Root.Descendants("Company")
                .Select(p => new Company()
                {
                    ID = int.Parse(p.Descendants("ID").First().Value),
                    Name = p.Descendants("CompanyName").First().Value,
                    Email = p.Descendants("Email").First().Value,
                    Address = p.Descendants("Address").First().Value,
                    Latitude = decimal.Parse(p.Descendants("Latitude").First().Value, CultureInfo.GetCultureInfo("en-US").NumberFormat),
                    Longitude = decimal.Parse(p.Descendants("Longitude").First().Value, CultureInfo.GetCultureInfo("en-US").NumberFormat),
                    DateFrom = DateTime.ParseExact(p.Descendants("DateFrom").First().Value, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    CityID = Math.Abs(p.Descendants("City").First().Value.GetHashCode())
                })
                .AsQueryable()
                .ToList();

            foreach (var node in allNodes)
                node.City = _cityRepository.FindByID(node.CityID);

            _cache = allNodes.ToList();

            return allNodes.AsQueryable();
        }

        public Company FindByID(int companyId)
        {
            return All().Where(p => p.ID == companyId)
                .FirstOrDefault();
        }

        public bool InsertOrUpdate(Company entity)
        {
            //This is just a mock repository
            return true;
        }

        public bool Delete(int companyId)
        {
            //This is just a mock repository
            return true;
        }
    }

}