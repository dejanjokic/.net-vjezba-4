﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.Web.Models.Mock;

namespace Vjezba.Web.Controllers
{
    [RoutePrefix("komp")]
    public class CompanyController : Controller
    {
        // GET: Company
        List<Company> Companies = new List<Company>();
        public ActionResult Index()
        {
            Companies = MockCompanyRepository.GetInstance().All().ToList();

            return View("Index", Companies);
        }

        public ActionResult Details(int id = 0)
        {
            Company Company = MockCompanyRepository.GetInstance().FindByID(id);

            return View("Details", Company);
        }

        [Route("pretraga/{s:alpha:length(2,5)}")]
        public ActionResult Search(String s)
        {
            Companies = MockCompanyRepository.GetInstance().All().ToList();

            List<Company> CompaniesFiltered = Companies.Where(c => c.Name.ToLower().Contains(s)).ToList();

            return View("Index", CompaniesFiltered);
        }

    }
}