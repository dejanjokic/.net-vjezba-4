﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Vjezba.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "Kompanije",
                url: "kompanije",
                defaults: new
                {
                    controller = "Company",
                    action = "Index"
                });

            routes.MapRoute(
                name: "KompanijaDetalji",
                url: "kompanija/detalji/{id}",
                defaults: new
                {
                    controller = "Company",
                    action = "Details",
                    id = UrlParameter.Optional },
                constraints: new
                {
                    id = @"[0-9]*"
                });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );            
        }
    }
}
